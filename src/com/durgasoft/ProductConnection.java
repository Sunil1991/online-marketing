package com.durgasoft;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
public class ProductConnection 
{
	
	ArrayList<Product> cost=new ArrayList<Product>();
	ArrayList<Integer> price=new ArrayList<Integer>();
	Connection con=null;
	Statement st=null;
	public Connection getConnection()
	{
	try
	{
		
		Class.forName("oracle.jdbc.OracleDriver");
	    con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","manager");
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return con;
	}
	public ArrayList<Integer> getProducts() throws SQLException
	{
		try
		{
			ProductConnection pc=new ProductConnection();
			Connection con=pc.getConnection();
			st=con.createStatement();
			ResultSet rs=st.executeQuery("select * from mproduct");
			while(rs.next())
            {
            	Product p=new Product();
            	p.setPid(rs.getInt(1));
            	p.setPname(rs.getString(2));
            	p.setPcost(rs.getInt(3));
            	price.add(rs.getInt(3));
            }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		System.out.println(price);
		return price ;
		
	}
	public int[] buildIntArray(ArrayList<Integer> integers) 
	{
	    int[] ints = new int[integers.size()];
	    int i = 0;
	    for (Integer n : integers) 
	    {
	        ints[i++] = n;
	    }
	    return ints;
	}
}	

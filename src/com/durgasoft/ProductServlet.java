package com.durgasoft;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class ProductServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		HttpSession hs=request.getSession();
		String name=(String)hs.getAttribute("name");
		int ccost=Integer.parseInt(request.getParameter("CCOST"));
		int sycost=Integer.parseInt(request.getParameter("SYCOST"));
	    int mcost=Integer.parseInt(request.getParameter("MCOST"));
	    int scost=Integer.parseInt(request.getParameter("SCOST"));
	    int hcost=Integer.parseInt(request.getParameter("HCOST"));
	    /*out.println("Celkon Cost:"+ccost);
	    out.println("Sony Cost:"+sycost);
	    out.println("Motorola Cost:"+mcost);
	    out.println("Samsung Cost:"+scost);
	    out.println("HTC Cost:"+hcost);*/
	    ArrayList<Integer> qty=new ArrayList<Integer>();
	    qty.add(ccost);
	    qty.add(sycost);
	    qty.add(mcost);
	    qty.add(scost);
	    qty.add(hcost);
	    ProductConnection pc=new ProductConnection();
	    ArrayList<Integer> cost=new ArrayList<Integer>();
	    try {
			cost=pc.getProducts();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    //int [] a1=pc.buildIntArray(cost);
	    //int [] a2=pc.buildIntArray(qty);
	    ArrayList<Integer> mul=new ArrayList<Integer>();
	    int count=0;
	    for(int i=0;i<=4;i++)
	    {
	    	mul.add(qty.get(i)*cost.get(i));
	    	count=count+mul.get(i);
	    }
	    hs.setAttribute("name",name);
	    hs.setAttribute("qty",qty);
	    hs.setAttribute("cost",cost);
	    hs.setAttribute("mul",mul);
	    hs.setAttribute("count",count);
	    RequestDispatcher rd=request.getRequestDispatcher("billinghome.jsp");
	    rd.forward(request,response);
	    }
	}
	    


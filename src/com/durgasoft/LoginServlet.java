package com.durgasoft;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class LoginServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		ProductConnection pc=new ProductConnection();
		Connection con=pc.getConnection();
		PreparedStatement pst=null;
		HttpSession hs=request.getSession();
		/*Connection con=null;
		
		try 
		{
			Class.forName("oracle.jdbc.OracleDriver");
			 con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","manager");
		}
		catch (ClassNotFoundException | SQLException e1) 
		{
			e1.printStackTrace();
		}*/
		String hiddenParam=request.getParameter("pagename");
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		if(hiddenParam.equals("Register"))
		{
			String password=request.getParameter("txtPassword");
			String fname=request.getParameter("fname");
			String lname=request.getParameter("lname");
			String gender=request.getParameter("gender");
			String age=request.getParameter("cboAge");
			String email=request.getParameter("email");
			String state=request.getParameter("cboState");
			String address=request.getParameter("txtAddress");
			String skill=request.getParameter("skill");
			GetsSets sets=new GetsSets();
			sets.setFname(fname);
			sets.setLname(lname);
			sets.setGender(gender);
			sets.setAge(age);
			sets.setEmail(email);
			sets.setState(state);
			sets.setPassword(password);
			sets.setAddress(address);
			sets.setSkill(skill);
			try 
			{
				int t=DbManager.insert(sets,pst,con);
				if(t==0)
				{
					hs.setAttribute("fname",fname);
					RequestDispatcher rd=request.getRequestDispatcher("sucessful.jsp");
					rd.forward(request,response);
				}
			}
			catch(ClassNotFoundException |SQLException e)
			{
				e.printStackTrace();
			}
			
		}
		if (hiddenParam.equals("Login"))
		{	
			String email=request.getParameter("txtUsername");
			String pwd=request.getParameter("txtPassword");
			if(email.equals("admin@gmail")&&pwd.equals("admin"))
			{		
		   hs.setAttribute("admin",email);
	       RequestDispatcher rd = request.getRequestDispatcher("productstock.jsp");
	       rd.forward(request, response);
			}
			else
			{
			GetsSets set=new GetsSets();
			set.setEmail(email);
			set.setPassword(pwd);
			try 
			{
			String name=DbManager.checkUser(set,pst,con);
			out.print(name);
			hs.setAttribute("name",name);
		      /*if(checkUser==1)
					{
			       hs.setAttribute("name",email);
				   RequestDispatcher rd = request.getRequestDispatcher("productjspform.jsp");
				   rd.forward(request, response);
					}
				*/					

		//	else
			//        {
						
			   RequestDispatcher rd = request.getRequestDispatcher("productform.jsp");
			  rd.forward(request, response);
			//	}
			}
			
			catch (SQLException e) 
					{
						e.printStackTrace();
					}
					
			catch (ClassNotFoundException e) 
			{
						e.printStackTrace();
			}
}
		}
	}
}